variable "name" {
  description = "The base name to assign to this S3 bucket. The actual bucket name will include a serial suffix."
  type        = string
}

variable "tags" {
  default     = {}
  description = "Tags to be set on all resources provisioned by this module."
  type        = map(string)
}
