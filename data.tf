data "aws_iam_policy_document" "reader" {
  statement {
    actions = [
      "kms:Decrypt"
    ]
    effect    = "Allow"
    resources = [aws_kms_key.storage.arn]
    sid       = "Decrypt"
  }

  statement {
    actions = [
      "s3:ListBucket",
      "s3:ListBucketVersions"
    ]
    effect    = "Allow"
    resources = [aws_s3_bucket.storage.arn]
    sid       = "ReadBucket"
  }

  statement {
    actions = [
      "s3:GetObject",
      "s3:GetObjectVersion",
    ]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.storage.arn}/*"]
    sid       = "ReadObjects"
  }
}

data "aws_iam_policy_document" "writer" {
  source_policy_documents = [data.aws_iam_policy_document.reader.json]

  statement {
    actions = [
      "kms:Encrypt",
      "kms:GenerateDataKey"
    ]
    effect    = "Allow"
    resources = [aws_kms_key.storage.arn]
    sid       = "Encrypt"
  }

  statement {
    actions = [
      "s3:ListBucketMultipartUploads"
    ]
    effect    = "Allow"
    resources = [aws_s3_bucket.storage.arn]
    sid       = "BucketLevel"
  }

  statement {
    actions = [
      "s3:AbortMultipartUpload",
      "s3:DeleteObject",
      "s3:DeleteObjectVersion",
      "s3:ListMultipartUploadParts",
      "s3:PutObject",
      "s3:PutObjectAcl",
      "s3:PutObjectVersionAcl"
    ]
    effect    = "Allow"
    resources = ["${aws_s3_bucket.storage.arn}/*"]
    sid       = "WriteObjects"
  }
}
