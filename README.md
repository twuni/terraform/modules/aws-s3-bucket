# S3 Bucket | AWS | Terraform Modules | Twuni

This Terraform module provisions a private AWS S3 bucket, suitable
for use for storing sensitive data. Encryption is enabled using a
dedicated KMS key -- also provisioned by this module -- and access
is limited by two IAM policies: one granting read-only access, and
another granting read/write access. These policies are not assigned
to any entities by this module -- instead, their ARNs are included
as outputs. In your code, you may attach these to the suitable IAM
users and/or roles.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.0.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.52.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_name"></a> [name](#input\_name) | The base name to assign to this S3 bucket. The actual bucket name will include a serial suffix. | `string` | n/a | yes |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags to be set on all resources provisioned by this module. | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_bucket_name"></a> [bucket\_name](#output\_bucket\_name) | The actual name of the S3 bucket provisioned by this module. |
| <a name="output_reader_policy_arn"></a> [reader\_policy\_arn](#output\_reader\_policy\_arn) | The ARN of an IAM policy granting read-only access to this S3 bucket. |
| <a name="output_writer_policy_arn"></a> [writer\_policy\_arn](#output\_writer\_policy\_arn) | The ARN of an IAM policy granting read/write access to this S3 bucket. |
