output "bucket_name" {
  description = "The actual name of the S3 bucket provisioned by this module."
  value       = aws_s3_bucket.storage.id
}

output "reader_policy_arn" {
  description = "The ARN of an IAM policy granting read-only access to this S3 bucket."
  value       = aws_iam_policy.reader.arn
}

output "writer_policy_arn" {
  description = "The ARN of an IAM policy granting read/write access to this S3 bucket."
  value       = aws_iam_policy.writer.arn
}
