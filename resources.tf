resource "aws_kms_key" "storage" {
  key_usage                = "ENCRYPT_DECRYPT"
  customer_master_key_spec = "SYMMETRIC_DEFAULT"
  is_enabled               = true
  enable_key_rotation      = false
  tags = merge(var.tags, {
    Name = "${var.name}-s3-bucket"
  })
}

resource "aws_s3_bucket" "storage" {
  bucket_prefix = "${var.name}-"
  acl           = "private"
  force_destroy = true

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.storage.arn
        sse_algorithm     = "aws:kms"
      }
    }
  }

  versioning {
    enabled = true
  }

  tags = merge(var.tags, {
    Name = var.name
  })
}

resource "aws_s3_bucket_public_access_block" "storage" {
  bucket                  = aws_s3_bucket.storage.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_iam_policy" "reader" {
  name_prefix = "${var.name}-storage-reader-"
  path        = "/"
  policy      = data.aws_iam_policy_document.reader.json

  tags = merge(var.tags, {
    Name = "${var.name}-s3-bucket-reader"
  })
}

resource "aws_iam_policy" "writer" {
  name_prefix = "${var.name}-storage-writer-"
  path        = "/"
  policy      = data.aws_iam_policy_document.writer.json

  tags = merge(var.tags, {
    Name = "${var.name}-s3-bucket-writer"
  })
}
